
/*
   flow_monitor.ino

   Jacob Kunnappally
   September 2017

   What this program does:

   Setup
   -Define pin constants and
    flow rate limit
   -Declare and initialize utility globals
   -Set encoder connected pins to input
   -Set feedback LED pins as output
   -Open serial comms

   Runtime
   -Look for 16 encoder transitions
    (1 full rotation, break on timeout)
   -calculate time to complete rotation
   -check for reverse flow (do numbers show up in order?)
   -convert rotation period to flow rate
    with constant 1rotation/oz factor
   -determine whether flow rate is over limit
   -print to serial
*/

#define LSB 8
#define MSB 9
#define OVERFLOW_LED 10
#define BACKFLOW_LED 11
#define RATE_LIMIT 10

static int t0, dt, enc_val, prev_val, secs;
static float gpm;
static bool noflow, backflow, overflow;

bool change_wait(int prev_val);
//int debounce(int val);

void setup()
{
  t0 = 0;
  dt = 0;
  enc_val = 0;
  prev_val = 0;
  secs = 0;
  noflow = false;
  backflow = false;

  pinMode(LSB, INPUT);
  pinMode(MSB, INPUT);
  pinMode(OVERFLOW_LED, OUTPUT);
  pinMode(BACKFLOW_LED, OUTPUT);

  Serial.begin(115200);
  while (!Serial) delay(100);
  Serial.flush();
  Serial.println("Starting...");
  delay(500);
}

void loop()
{
  t0 = micros();

  for (int i = 0; i < 4; i++)
  {
    for (int j = 0; j < 4; j++)
    {
      //decode to decimal
      enc_val = digitalRead(LSB) | digitalRead(MSB) << 1;
      prev_val = enc_val;

      // encoder mechanical "fix" makes this unnecessary
      //       debounce(change_wait(prev_val));

      noflow = change_wait(prev_val);//backflow set here also

      if (noflow)
        break;
    }

    digitalWrite(BACKFLOW_LED, backflow);

    if (noflow)
      break;
  }

  dt = micros() - t0;//rotation time in us

  if (noflow)
    gpm = 0.0;
  else
    gpm = 468750.0 / dt;
  //(1 gallon = 128 ounces, 1 rotation -> 1 ounce)
  //+dimensional analysis

  overflow = (gpm > RATE_LIMIT);
  digitalWrite(OVERFLOW_LED, overflow);

  //print gallons per second and
  //direction every second
  if ((micros() - secs) > 1000000)
  {
    if (overflow)
    {
      Serial.print("!!!WARNING !!! FLOW RATE ");
      Serial.print(RATE_LIMIT);
      Serial.println(" GAL/m EXCEEDED !!!");
    }

    Serial.print("Flow Rate: ");
    Serial.print(gpm);
    Serial.println(" gallons per minute");
    Serial.print("Direction: ");
    switch (backflow)
    {
      case true:
        Serial.println("Reverse. SHUTDOWN CONDITION");
        break;
      case false:
        Serial.println("Forward");
    }
    Serial.println("\n");

    secs = micros();
  }

}

bool change_wait(int prev_val)
{
  noflow = false;
  int timeout = micros();

  //wait for change
  while (enc_val == prev_val && !noflow)
  {
    enc_val = digitalRead(LSB) | digitalRead(MSB) << 1;

    if (micros() - timeout >= 1000000)
      noflow = true;
  }

  if ((prev_val == 0) && (enc_val == 2) ||
      (prev_val == 1) && (enc_val == 0) ||
      (prev_val == 3) && (enc_val == 1) ||
      (prev_val == 2) && (enc_val == 3))
    backflow = true;
  else
    backflow = false;

  return noflow;
}

//kept here for posterity
//int debounce(int new_val)
//{
//  int nobounce_count = 0;
//
//  //hold until settle
//  while (nobounce_count < 10)
//  {
//    enc_val = digitalRead(LSB) | digitalRead(MSB) << 1;
//
//    if (enc_val == new_val)
//    {
//      nobounce_count++;
//    }
//
//    delayMicroseconds(100);
//  }
//
//  return enc_val;
//}


